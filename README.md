## Hello, I am Alvin Au :wave:
<!--
<img  src="https://github-readme-streak-stats.herokuapp.com/?user=alvinau0427&hide_border=true&theme=dark" width="48%"  align="left">
<img  src="https://github-readme-stats.vercel.app/api?username=alvinau0427&show_icons=true&hide_border=true&theme=dark&title_color=FFA500&&icon_color=FFA500&text_color=FFFFFF" width="48%">
-->
<div align="center">
  <img width="60%" height="auto" src="https://gitlab.com/alvinau0427/alvinau0427/-/raw/master/img_github_banner_alvinau0427.png?ref_type=heads">
</div>

## Find me around the web :globe_with_meridians:
- :mortar_board: Graduated from The Open University of Hong Kong
- :computer: Bachelor of Computing with Honours in Internet Technology
- :mag_right: Know more about me on [Portfolio](https://alvinau0427.github.io/) or [Blog](https://alvinau0427-blog.netlify.app/)
- :briefcase: Keep updates profile on [LinkedIn](https://www.linkedin.com/in/alvinau0427/)
- :email: Reach me by email to alvinau0427@outlook.com